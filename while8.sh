#!/bin/bash

#while format

a=10;
while [ $a -gt 1 ]
do 
 echo "$a"
 a=$(( $a/2 - 1 ))
done
####################
#multiple conditions

a=10
b=10
while [ $a -ge 0 ] 
      [ $b -ge 0 ]
do
 echo "$a - $b"
 a=$[ $a - 1 ]
 b=$[ $b -1 ]
done
