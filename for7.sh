#!/bin/bash
#lists="bhargav kumar reddy"
lists="bhargav kum:ar;reddy"
IFS=\;'\n'\:
for list in $lists
do
 echo $list
done
###########
#wildcards and multiple entries

for file in $HOME/* /etc/bhargav
do
  if [ -d "$file" ]
  then
     echo "$file is a directory"
  elif [ -f "$file" ]
  then
    echo "$file is a file"
  else
    echo "$file doesnt exist"
  fi
done

###########################
# c style for loop is supported by bash

for (( i=1; i<10; i++ ))
do
  echo $i
done

###################
# c style allows you multiple variables but single condition

for (( a=1,b=10; a<=10; a++,b-- ))
do 
 echo "$a - $b"
done
