#!/bin/bash
#using test command
user=''
if [ $user ]
then 
  echo "$user has been passed as user variable"
else
  echo "user variable is not defined"
fi

#using numeric comparison
var1=10
var2=20
if [ $var1 -eq $var2 ]
then
  echo "$var1 is equal to $var2"
else
  echo "$var1 not equal to $var2"
fi

#using string comparisone
str1=bhargav
str2=kumar
if [ $str1 \> $str2 ]
then
  echo "greater"
elif  [ -n $str1 ]
then
  echo "not greater"
  echo "str1 is not empty"
fi

#using file comparison
if [ -d /etc ]
then
  echo "etc exists and directory"
fi
if [ -x /Users/bhargavreddy/scripts/if.sh ]
then
 echo "script exists and executable"
fi
