#!/bin/bash

#For loop inside for loop

echo "starting for for loop inside for loop"

for (( a=10; a < 15; a++ ))
do 
  echo $a
  for (( b = 5; b > 3; b-- ))
  do 
    echo $b
  done
done

echo "starting for loop inside while loop"

c=4
while [ $c -gt 0 ]
do 
  echo $c
  for value in $c
  do 
    echo "inside loop c value is $c"
  done
  c=$[ $c - 1 ]
done


