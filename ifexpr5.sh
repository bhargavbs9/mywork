#!/bin/bash

#to test expr in if clause

a=100
b=5
if (( $b / $a > 10 ))
then
  echo "greater than 10"
else
  echo "not greater than 10"
fi


user="bhargav"
if [[ $user == b* ]]
then 
  echo "starting with b"
fi 
